# `obj2vtk`

Converts an `obj` file into a `vtp` file. This code was written by [Gilles
Daviet](http://gdaviet.fr/).

## Build

`obj2vtk` depends on VTK, it should be available in the package repositories of
your Linux distribution.

To build the software run
```
mkdir build
cd build
cmake ..
make
```

## Usage

To run the software just run
```
./obj2vtk path/to/file...
```
The resulting `vtp` files will be put in the same directory as the files passed
as arguments with the extension `.obj` replaced with `.vtp`.

The software can take more than one file as input. Hence, the following command
will convert every file in the directory `foo`
```
./obj2vtk foo/*.obj
```
