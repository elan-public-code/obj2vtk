#include <vtkVersion.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkOBJReader.h>
#include <vtkPolyDataMapper.h>

#include <string>

int main (int argc, const char *argv[])
{
  // Parse command line arguments
  if(argc < 2) {
    std::cout << "Usage: " << argv[0] << " Filename.obj [File2.obj ...]  " << std::endl;
    return EXIT_FAILURE;
  }

  vtkSmartPointer<vtkOBJReader> reader =
    vtkSmartPointer<vtkOBJReader>::New();
  
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(reader->GetOutputPort());
  
  vtkSmartPointer<vtkXMLPolyDataWriter> writer =  
    vtkSmartPointer<vtkXMLPolyDataWriter>::New();
  
  for( unsigned i = 1 ; i < argc ; ++ i)
  {
  
    const std::string ifn(argv[i]) ;

    reader->SetFileName(ifn.c_str());
    reader->Update();
   
    const std::string dest = ifn.substr(0, ifn.rfind('.')) + ".vtp" ;

    std::cout << ifn << "\t => " << dest << std::endl ;

    // Write the file
    writer->SetFileName(dest.c_str());
#if VTK_MAJOR_VERSION <= 5
    writer->SetInput(mapper->GetInput());
#else
    writer->SetInputData(mapper->GetInput());
#endif
   
    // Optional - set the mode. The default is binary.
    //writer->SetDataModeToBinary();
    //writer->SetDataModeToAscii();
   
    writer->Write();
  }
 
  return EXIT_SUCCESS;
}
